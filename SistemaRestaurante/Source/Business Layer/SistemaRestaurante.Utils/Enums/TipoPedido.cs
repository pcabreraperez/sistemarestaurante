﻿

namespace SistemaRestaurante.Utils.Enums
{
    public enum TipoPedido : int
    {
        Delivery = 0,
        Local = 1,
        Tipo3 = 3,
        Tipo4 = 4
    }
}
