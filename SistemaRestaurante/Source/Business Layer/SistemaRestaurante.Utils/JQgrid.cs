﻿using System;

namespace SistemaRestaurante.Utils
{
    [Serializable]
    public class JQgrid
    {
        public int total;
        public int page;
        public int records;
        public int start;
        public JRow[] rows;
    }


    [Serializable]
    public class JRow
    {
        public int id;
        public string[] cell;
    }
}