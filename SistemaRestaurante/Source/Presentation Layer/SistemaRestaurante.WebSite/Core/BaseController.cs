﻿

namespace SistemaRestaurante.WebSite.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Utils;
    using Newtonsoft.Json;
    using Microsoft.Reporting.WebForms;
    

    [HandleError]
    [Authorize]
    public class BaseController : Controller
    {
       
       #region Metodos

        protected JsonResult MensajeError(string mensaje = "Ocurrio un error.")
        {
            Response.StatusCode = 404;
            return Json(new JsonResponse { Message = mensaje }, JsonRequestBehavior.AllowGet);
        }


        #endregion Metodos

        #region RenderReport

        public void RenderReport(string report, string ds, object data, string formato)
        {
            var reportPath = Server.MapPath("~/Reportes/" + report + ".rdlc");
            var localReport = new LocalReport { ReportPath = reportPath };
            var reportDataSource = new ReportDataSource(ds, data);

            localReport.DataSources.Add(reportDataSource);

            var reportType = string.Empty;
            var deviceInfo = string.Empty;

            switch (formato)
            {
                case "PDF":
                    reportType = "PDF";
                    deviceInfo = string.Format("<DeviceInfo><OutputFormat>{0}</OutputFormat><PageWidth>8.5in</PageWidth><PageHeight>11in</PageHeight><MarginTop>0.5in</MarginTop><MarginLeft>0.5in</MarginLeft><MarginRight>0.5in</MarginRight><MarginBottom>0.5in</MarginBottom></DeviceInfo>", reportType);
                    break;
                case "EXCEL":
                    reportType = "Excel";
                    break;
                default:
                    return;
            }

            string mimeType, encoding, fileNameExtension;
            Warning[] warnings;
            string[] streams;

            var renderedBytes = localReport.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + report + "." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }

        #endregion RenderReport
    }
}
