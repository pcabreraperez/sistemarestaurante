﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistemaRestaurante.WebSite.Controllers
{
    using SistemaRestaurante.BusinessEntity;
    using SistemaRestaurante.BusinessLogic;
      [Authorize]
    public class ClienteController : Controller
    {
        //
        // GET: /Cliente/

        public ActionResult Index()
        {
            var lista = ClienteBL.Instancia.Listar();
            return View("Listado", lista);
        }

        public ActionResult Create()
        {
            var cliente = new Cliente();

            return View("Create", cliente);
        }


        [HttpPost]
        public ActionResult Create(Cliente cliente)
        {
            ClienteBL.Instancia.Agregar(cliente);

            var lista = ClienteBL.Instancia.Listar();
            return View("Listado", lista);
        }

        public ActionResult Edit(int idCliente)
        {
            var cliente = ClienteBL.Instancia.Obtener(idCliente);

            return View("Edit", cliente);

        }

        [HttpPost]
        public ActionResult Edit(Cliente cliente)
        {
            ClienteBL.Instancia.Modificar(cliente);

            var lista = ClienteBL.Instancia.Listar();
            return View("Listado", lista);
        }

        public ActionResult Delete(int idCliente)
        {
            var cliente = ClienteBL.Instancia.Obtener(idCliente);

            return View("Delete", cliente);
        }

        [HttpPost]
        public ActionResult Delete(Cliente cliente)
        {
            ClienteBL.Instancia.Eliminar(cliente.IdCliente);

            var lista = ClienteBL.Instancia.Listar();
            return View("Listado", lista);
        }

        public ActionResult Details(int idCliente)
        {
            var cliente = ClienteBL.Instancia.Obtener(idCliente);

            return View("Details", cliente);
        }

    }
}
